﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Google.AR.Core;
using Google.AR.Sceneform;
using Google.AR.Sceneform.Rendering;
using Google.AR.Sceneform.UX;

namespace SceneformModel
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private ArFragment arFragment;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.content_main);

            arFragment = (ArFragment) SupportFragmentManager.FindFragmentById(Resource.Id.ux_fragment);
            arFragment.TapArPlane += ArFragment_TapArPlane;
        }

        private void ArFragment_TapArPlane(object sender, BaseArFragment.TapArPlaneEventArgs e)
        {
            Anchor anchor = e.HitResult.CreateAnchor();
            AnchorNode anchorNode = new AnchorNode(anchor);
            anchorNode.SetParent(arFragment.ArSceneView.Scene);

            
            ModelRenderable.InvokeBuilder().SetSource(this, 
                //Android.Net.Uri.Parse("aircrafttest.sfb")
                Resource.Raw.kokosh
                ).Build((renderable) =>
            {
                // Create the transformable andy and add it to the anchor.
                TransformableNode andy = new TransformableNode(arFragment.TransformationSystem);
                andy.SetParent(anchorNode);
                andy.Renderable = renderable;
                andy.Select();
            });
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
	}
}
